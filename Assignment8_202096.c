#include<stdio.h>
#include <string.h>
#define SIZE 100

int i=0, numofstudents;

struct details
{
    char firstname[SIZE];
    char subject[SIZE];
    int marks;
};

struct details arr1[SIZE];

int main()
{

   	do{
   		printf ("How many students are there? ");
   		scanf ("%d",&numofstudents);

   		if (numofstudents < 5){
   			printf("Minimum number of students can be added to the system is 5. So, try again!\n\n");
   		}
   	}while(numofstudents < 5);

    for (i=0;i<numofstudents;i++)
   	{
        printf ("\nGive the name of student%d :",i+1);
        scanf ("%s",&arr1[i].firstname);
        printf ("What is the subject he/she did : ");
        scanf ("%s",&arr1[i].subject);
        printf ("Enter marks : ");
        scanf ("%d",&arr1[i].marks);
    }
	printf ("\n\n");

    for (i=0;i<numofstudents;i++)
    {
        printf ("%s's marks for %s is %d.\n",arr1[i].firstname,arr1[i].subject,arr1[i].marks);
    }

    return 0;
}
