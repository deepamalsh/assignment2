#include <stdio.h>

void pattern(int);
void rows(int);

int main(){
    int number;
    printf("How many rows should print? ");
    scanf("%d",&number);
    rows(number);
    return 0;
}
void pattern(int n){
    if (n>0){
        printf("%d",n);
        pattern(n-1);}
}
void rows(int n){
    if(n>0){
        rows(n-1);
        printf("\n");
        pattern(n);}
}
