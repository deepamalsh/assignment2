#include <stdio.h>
#include <stdlib.h>

int main()
{
    int sum=0;
    int num,x;
    printf ("Enter a number : ");
    scanf ("%d",&num);
    while (num!=0){
        x=num%10;
        sum=sum*10+x;
        num=num/10;
    }
    printf ("Reversed number = %d",sum);
    return 0;
}
