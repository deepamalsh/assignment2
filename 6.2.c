#include <stdio.h>

int fibonacciSeq(int n);
int pattern (int n);

int main(){
    int number;
    printf("print the fibonacci sequence up to?");
    scanf("%d",&number);
    printf("%d",pattern (number));
    return 0;
}

int fibonacciSeq(int n){
    if (n==0)
        return 0;
    else if (n==1)
        return 1;
    else
        return fibonacciSeq(n-1)+ fibonacciSeq(n-2);
}

int pattern (int n){
    if (n>0){
        printf("%d",pattern(n-1));
        printf("\n");
        fibonacciSeq(n);
    }
    else
        return 0;
}
