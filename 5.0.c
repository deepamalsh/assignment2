#include<stdio.h>

//Function to count number of attendees
int attendees(int priceofaticket);

//Function to calculate the revenue
int revenue(int priceofaticket);

//Function to calculate the total cost
int cost(int priceofaticket);

//Function to calculate the profit
int profit(int priceofaticket);


int	attendees(int priceofaticket){
	return 120-(priceofaticket-15)/5*20;}

int revenue(int priceofaticket){
	return attendees(priceofaticket)*priceofaticket;}

int cost(int priceofaticket){
	return attendees(priceofaticket)*3+500;}

int profit(int priceofaticket){
	return revenue(priceofaticket)-cost(priceofaticket);}

int main(){
	int priceofaticket;
		for(priceofaticket=5;priceofaticket<50;priceofaticket+=5){
        printf("When ticket price is Rs.%d.00, he can take Rs.%d.00 profit.\n",priceofaticket,profit(priceofaticket));}
		return 0;
	}
